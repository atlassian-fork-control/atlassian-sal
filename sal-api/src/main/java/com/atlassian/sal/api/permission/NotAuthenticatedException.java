package com.atlassian.sal.api.permission;

/**
 * Exception that is thrown by one of the enforce methods in {@link PermissionEnforcer} when the current user does not
 * have the required level of access because the user is not authenticated.
 *
 * @since 3.2
 */
public class NotAuthenticatedException extends AuthorisationException {

    public NotAuthenticatedException() {
    }

    public NotAuthenticatedException(String message) {
        super(message);
    }

    public NotAuthenticatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAuthenticatedException(Throwable cause) {
        super(cause);
    }
}
