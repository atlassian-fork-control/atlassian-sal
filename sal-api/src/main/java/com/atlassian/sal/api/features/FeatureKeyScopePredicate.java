package com.atlassian.sal.api.features;

import com.google.common.base.Predicate;

import javax.annotation.Nullable;

/**
 * @since 2.10.1
 */
public class FeatureKeyScopePredicate implements Predicate<FeatureKeyScope> {
    private final FeatureKeyScope featureKeyScope;

    public FeatureKeyScopePredicate(final FeatureKeyScope featureKeyScope) {
        this.featureKeyScope = featureKeyScope;
    }

    public static FeatureKeyScopePredicate filterBy(final FeatureKeyScope featureKeyScope) {
        return new FeatureKeyScopePredicate(featureKeyScope);
    }

    @Override
    public boolean apply(@Nullable final FeatureKeyScope input) {
        return input != null && input == featureKeyScope;
    }
}
