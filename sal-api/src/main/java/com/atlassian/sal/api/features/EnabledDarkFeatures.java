package com.atlassian.sal.api.features;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a set of enabled features.
 *
 * @since 2.10
 */
@Immutable
public final class EnabledDarkFeatures {
    /**
     * Shorthand in case there are no enabled dark features or they are disabled
     */
    public static final EnabledDarkFeatures NONE = new EnabledDarkFeatures(ImmutableMap.<FeatureKeyScope, ImmutableSet<String>>of());

    private final ImmutableMap<FeatureKeyScope, ImmutableSet<String>> enabledFeatures;

    /**
     * Create an instance of enabled features based on the given map.
     *
     * @param enabledFeatures a map of enabled features
     */
    public EnabledDarkFeatures(final ImmutableMap<FeatureKeyScope, ImmutableSet<String>> enabledFeatures) {
        checkNotNull(enabledFeatures, "enabledFeatures");
        this.enabledFeatures = ImmutableMap.copyOf(enabledFeatures);
    }

    /**
     * Return all enabled feature keys.
     *
     * @return all enabled feature keys
     */
    public ImmutableSet<String> getFeatureKeys() {
        return ImmutableSet.copyOf(Iterables.concat(enabledFeatures.values()));
    }

    /**
     * Returns all enabled feature keys matching the given criteria.
     *
     * @param criteria the filter condition to be applied on the set of enabled features
     * @return the filtered set of enabled features
     */
    public ImmutableSet<String> getFeatureKeys(final Predicate<FeatureKeyScope> criteria) {
        checkNotNull(criteria, "criteria");
        return ImmutableSet.copyOf(Iterables.concat(Maps.filterKeys(enabledFeatures, criteria).values()));
    }

    /**
     * Check that the given feature is enabled
     *
     * @param featureKey the feature key to be checked
     * @return <code>true</code> if the given feature key is enabled, <code>false</code> otherwise
     */
    public boolean isFeatureEnabled(final String featureKey) {
        checkNotNull(featureKey, "featureKey");
        return Iterables.contains(Iterables.concat(enabledFeatures.values()), featureKey);
    }

    @Override
    public String toString() {
        return "EnabledDarkFeatures{enabledFeatures=" + enabledFeatures + '}';
    }

}
