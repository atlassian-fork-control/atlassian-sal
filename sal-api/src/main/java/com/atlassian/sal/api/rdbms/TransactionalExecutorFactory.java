package com.atlassian.sal.api.rdbms;

import com.atlassian.annotations.PublicApi;

/**
 * Provided by the host application for creating {@link com.atlassian.sal.api.rdbms.TransactionalExecutor}.
 * <p>
 * Note that the TransactionalExecutors created are not considered thread safe.
 *
 * @since 3.0
 */
@PublicApi
public interface TransactionalExecutorFactory {

    /**
     * Create a transactional executor with <code>readOnly</code> not set and <code>requiresNew</code> not set
     */
    default TransactionalExecutor create() {
        return createExecutor(false, false);
    }

    /**
     * Create a transactional executor with <code>readOnly</code> set and <code>requiresNew</code> not set
     */
    default TransactionalExecutor createReadOnly() {
        return createExecutor(true, false);
    }

    /**
     * Create a transactional executor
     *
     * @param readOnly    initial value for <code>readOnly</code>
     * @param requiresNew initial value for <code>requiresNew</code>
     */
    default TransactionalExecutor createExecutor(boolean readOnly, boolean requiresNew) {
        throw new UnsupportedOperationException("not implemented by default");
    }

    /**
     * @deprecated As of release 3.1.0, replaced by {@link #createReadOnly()}
     * Create a transactional executor with <code>readOnly</code> set and <code>requiresNew</code> not set
     */
    @Deprecated
    default TransactionalExecutor createExecutor() {
        return createExecutor(true, false);
    }
}
