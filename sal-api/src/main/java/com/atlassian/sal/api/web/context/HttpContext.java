package com.atlassian.sal.api.web.context;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Provides access to the key objects provided by the servlet API when processing an HTTP request.
 * <p>
 * Use this interface rather than making static calls to classes like {@code ServletActionContext} directly.
 * <p>
 * Note that this interface makes no guarantees about which wrapper for the active request, response or session will be returned.
 * Callers should not rely on retrieving any particular wrapper. It is only guaranteed to be populated on a http request thread,
 * after login, and before decoration.
 *
 * @since 2.8
 */
public interface HttpContext {
    /**
     * Returns the active HTTP request or {@code null} if one cannot be found.
     */
    @Nullable
    HttpServletRequest getRequest();

    /**
     * Returns the active HTTP response or {@code null} if one cannot be found.
     */
    @Nullable
    HttpServletResponse getResponse();

    /**
     * Returns the session associated with the active request or, if there is no current session and {@code create} is true,
     * returns a new session.
     *
     * @param create should be {@code true} to create a new session for the active request or {@code false} to return
     *               {@code null} if there is no current session
     * @return the HttpSession associated with this request or {@code null} if {@code create} is false and the request has
     * no session, or if there is no active request
     */
    @Nullable
    HttpSession getSession(boolean create);
}
