package com.atlassian.sal.api.features;

/**
 * @since 2.10
 */
public class MissingPermissionException extends RuntimeException {
    public MissingPermissionException(String message) {
        super(message);
    }
}
