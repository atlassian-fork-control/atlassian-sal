package com.atlassian.sal.api.permission;

/**
 * Exception that is thrown by one of the enforce methods in {@link PermissionEnforcer} when the current user does not
 * have the required level of access.
 *
 * @since 3.2
 */
public class AuthorisationException extends RuntimeException {

    public AuthorisationException() {
        super();
    }

    public AuthorisationException(String message) {
        super(message);
    }

    public AuthorisationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorisationException(Throwable cause) {
        super(cause);
    }
}
