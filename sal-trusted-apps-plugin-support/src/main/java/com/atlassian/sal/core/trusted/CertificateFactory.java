package com.atlassian.sal.core.trusted;

import com.atlassian.security.auth.trustedapps.EncryptedCertificate;

/**
 * Interface for retrieving encrypted certificate for given username.
 *
 * @deprecated since v2.13.1 Trusted Apps functionality is being deprecated across all products.
 * Existing functionality will no longer be provided by the products moving forward.
 * As an interim measure, it will instead it will all be provided via plugins.
 */
@Deprecated
public interface CertificateFactory {
    /**
     * Trusted apps &gt; 3.0.0 requires a signature which in turn requires a url.
     *
     * @deprecated since 2.10.9 use createCertificate(String username, String url)
     */
    @Deprecated
    EncryptedCertificate createCertificate(String username);

    /**
     * Create a Trusted Apps certificate.
     * This will include a signature based the username and url.
     */
    EncryptedCertificate createCertificate(String username, String url);
}
