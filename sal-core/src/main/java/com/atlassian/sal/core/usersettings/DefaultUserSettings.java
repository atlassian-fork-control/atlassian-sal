package com.atlassian.sal.core.usersettings;

import io.atlassian.fugue.Option;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.google.common.base.Preconditions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.sal.api.usersettings.UserSettingsService.MAX_KEY_LENGTH;
import static com.atlassian.sal.api.usersettings.UserSettingsService.MAX_STRING_VALUE_LENGTH;

public class DefaultUserSettings implements UserSettings {
    private final Map<String, Object> settings;

    private DefaultUserSettings(Map<String, Object> settings) {
        this.settings = settings;
    }

    @Override
    public Option<String> getString(String key) {
        checkKeyArgument(key);

        if (!settings.containsKey(key)) {
            return Option.none();
        }

        final Object value = settings.get(key);
        return (value instanceof String) ? Option.some((String) value) : Option.none();

    }

    @Override
    public Option<Boolean> getBoolean(String key) {
        checkKeyArgument(key);

        if (!settings.containsKey(key)) {
            return Option.none();
        }

        final Object value = settings.get(key);
        return (value instanceof Boolean) ? Option.some((Boolean) value) : Option.none();
    }

    @Override
    public Option<Long> getLong(String key) {
        checkKeyArgument(key);

        if (!settings.containsKey(key)) {
            return Option.none();
        }

        final Object value = settings.get(key);
        return (value instanceof Long) ? Option.some((Long) value) : Option.none();

    }

    @Override
    public Set<String> getKeys() {
        return settings.keySet();
    }

    public static class Builder implements UserSettingsBuilder {
        private final Map<String, Object> settings = new HashMap<>();

        private Builder(UserSettings userSettings) {
            for (String key : userSettings.getKeys()) {
                for (Boolean value : userSettings.getBoolean(key)) {
                    settings.put(key, value);
                }
                for (String value : userSettings.getString(key)) {
                    settings.put(key, value);
                }
                for (Long value : userSettings.getLong(key)) {
                    settings.put(key, value);
                }
            }
        }

        private Builder() {

        }

        @Override
        public UserSettingsBuilder put(String key, String value) {
            checkKeyArgument(key);
            checkValueArgument(value);

            settings.put(key, value);
            return this;
        }

        @Override
        public UserSettingsBuilder put(String key, boolean value) {
            checkKeyArgument(key);

            settings.put(key, value);
            return this;
        }

        @Override
        public UserSettingsBuilder put(String key, long value) {
            checkKeyArgument(key);

            settings.put(key, value);
            return this;
        }

        @Override
        public UserSettingsBuilder remove(String key) {
            checkKeyArgument(key);

            settings.remove(key);
            return this;
        }

        @Override
        public Option<Object> get(String key) {
            checkKeyArgument(key);

            return settings.containsKey(key) ? Option.some(settings.get(key)) : Option.none();
        }

        @Override
        public Set<String> getKeys() {
            return settings.keySet();
        }

        @Override
        public UserSettings build() {
            return new DefaultUserSettings(settings);
        }

    }

    public static UserSettingsBuilder builder() {
        return new Builder();
    }

    public static UserSettingsBuilder builder(UserSettings userSettings) {
        return new Builder(userSettings);
    }

    private static void checkKeyArgument(String key) {
        Preconditions.checkArgument(key != null, "key cannot be null");
        Preconditions.checkArgument(key.length() <= MAX_KEY_LENGTH, "key cannot be longer than %s characters", MAX_KEY_LENGTH);
    }

    private static void checkValueArgument(String value) {
        Preconditions.checkArgument(value != null, "value cannot be null");
        Preconditions.checkArgument(value.length() <= MAX_STRING_VALUE_LENGTH, "value cannot be longer than %s characters", MAX_STRING_VALUE_LENGTH);
    }
}
