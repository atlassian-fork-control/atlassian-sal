package com.atlassian.sal.core.features;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.features.EnabledDarkFeaturesBuilder;
import com.atlassian.sal.api.features.MissingPermissionException;
import com.atlassian.sal.api.features.SiteDarkFeaturesStorage;
import com.atlassian.sal.api.features.ValidFeatureKeyPredicate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Optional;

import static com.atlassian.sal.api.features.ValidFeatureKeyPredicate.checkFeatureKey;

/**
 * Default implementation of DarkFeatureManager - sufficient for any product which does not already have its own dark
 * feature framework. Does not implement per-user enabling.
 */
public class DefaultDarkFeatureManager implements DarkFeatureManager {
    private final UserManager userManager;
    private final SiteDarkFeaturesStorage siteDarkFeaturesStorage;
    private final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures;

    public DefaultDarkFeatureManager(final UserManager userManager, final SiteDarkFeaturesStorage siteDarkFeaturesStorage) {
        this.userManager = userManager;
        this.siteDarkFeaturesStorage = siteDarkFeaturesStorage;
        this.systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();
    }

    @Override
    @Nonnull
    public Optional<Boolean> isEnabledForAllUsers(@Nonnull String featureKey) {
        if (!ValidFeatureKeyPredicate.isValidFeatureKey(featureKey)) {
            return Optional.empty();
        }
        if (systemDarkFeatures.getEnabled().contains(featureKey)
                || siteDarkFeaturesStorage.contains(featureKey)) {
            return Optional.of(Boolean.TRUE);
        }
        if (systemDarkFeatures.getDisabled().contains(featureKey)) {
            return Optional.of(Boolean.FALSE);
        }
        return Optional.empty();
    }

    @Override
    @Nonnull
    public Optional<Boolean> isEnabledForCurrentUser(@Nonnull String featureKey) {
        return isEnabledForAllUsers(featureKey);
    }

    @Override
    @Nonnull
    public Optional<Boolean> isEnabledForUser(@Nullable UserKey userKey, @Nonnull String featureKey) {
        if (isUserAnonymous(userKey) || isUserExisting(userKey)) {
            return isEnabledForAllUsers(featureKey);
        } else {
            throw new IllegalArgumentException("The user does not exist");
        }
    }

    @Override
    @Deprecated
    public boolean isFeatureEnabledForAllUsers(final String featureKey) {
        return ValidFeatureKeyPredicate.isValidFeatureKey(featureKey) && getFeaturesEnabledForAllUsers().isFeatureEnabled(featureKey);
    }

    @Override
    @Deprecated
    public boolean isFeatureEnabledForCurrentUser(final String featureKey) {
        return isFeatureEnabledForAllUsers(featureKey);
    }

    @Override
    @Deprecated
    public boolean isFeatureEnabledForUser(@Nullable final UserKey userKey, final String featureKey) {
        if (isUserAnonymous(userKey) || isUserExisting(userKey)) {
            return isFeatureEnabledForAllUsers(featureKey);
        } else {
            throw new IllegalArgumentException("The user does not exist");
        }
    }

    @Override
    public boolean canManageFeaturesForAllUsers() {
        try {
            final UserKey remoteUserKey = userManager.getRemoteUserKey();
            return userManager.isSystemAdmin(remoteUserKey);
        } catch (RuntimeException e) {
            /*
             * Applying the principle of least surprise here so the caller wouldn't have to deal with undeclared
             * runtime exceptions. Determining that a user has the proper permissions is very unlikely to be successful
             * when an exception is thrown.
             */
            return false;
        }
    }

    @Override
    public void enableFeatureForAllUsers(final String featureKey) {
        checkFeatureKey(featureKey);
        checkCurrentUserCanManageFeaturesForAllUsers();
        siteDarkFeaturesStorage.enable(featureKey);
    }

    @Override
    public void disableFeatureForAllUsers(final String featureKey) {
        checkFeatureKey(featureKey);
        checkCurrentUserCanManageFeaturesForAllUsers();
        siteDarkFeaturesStorage.disable(featureKey);
    }

    @Override
    public void enableFeatureForCurrentUser(final String featureKey) {
        throwUnsupportedPerUserOperationException();
    }

    @Override
    public void enableFeatureForUser(final UserKey userKey, final String featureKey) {
        throwUnsupportedPerUserOperationException();
    }

    @Override
    public void disableFeatureForCurrentUser(final String featureKey) {
        throwUnsupportedPerUserOperationException();
    }

    @Override
    public void disableFeatureForUser(final UserKey userKey, final String featureKey) {
        throwUnsupportedPerUserOperationException();
    }

    public EnabledDarkFeatures getFeaturesEnabledForAllUsers() {
        if (systemDarkFeatures.isDisableAll()) {
            return EnabledDarkFeatures.NONE;
        } else {
            return new EnabledDarkFeaturesBuilder()
                    .unmodifiableFeaturesEnabledForAllUsers(systemDarkFeatures.getEnabled())
                    .featuresEnabledForAllUsers(siteDarkFeaturesStorage.getEnabledDarkFeatures())
                    .build();
        }
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForCurrentUser() {
        return getFeaturesEnabledForAllUsers();
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForUser(@Nullable final UserKey userKey) {
        if (isUserAnonymous(userKey) || isUserExisting(userKey)) {
            return getFeaturesEnabledForAllUsers();
        } else {
            throw new IllegalArgumentException("The user does not exist");
        }
    }

    private boolean isUserExisting(@Nullable final UserKey userKey) {
        return (userKey != null) && userManager.getUserProfile(userKey) != null;
    }

    private boolean isUserAnonymous(@Nullable final UserKey userKey) {
        return userKey == null;
    }

    private void checkCurrentUserCanManageFeaturesForAllUsers() {
        if (!canManageFeaturesForAllUsers()) {
            throw new MissingPermissionException("The current user is not allowed to change dark features affecting all users.");
        }
    }

    private void throwUnsupportedPerUserOperationException() {
        throw new UnsupportedOperationException("The default implementation doesn't support per-user dark features.");
    }
}
