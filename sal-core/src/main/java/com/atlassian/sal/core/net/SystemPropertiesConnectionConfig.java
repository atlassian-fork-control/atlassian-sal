package com.atlassian.sal.core.net;

public class SystemPropertiesConnectionConfig implements ConnectionConfig {

    public static final String HTTP_SOCKET_TIMEOUT_PROPERTY_NAME = "http.socketTimeout";
    public static final String HTTP_CONNECTION_TIMEOUT_PROPERTY_NAME = "http.connectionTimeout";
    public static final String HTTP_MAX_REDIRECTS_PROPERTY_NAME = "http.max-redirects";

    public static final int DEFAULT_SOCKET_TIMEOUT = 10_000;
    public static final int DEFAULT_CONNECTION_TIMEOUT = 10_000;
    public static final int DEFAULT_MAX_REDIRECTS = 20;

    private final int socketTimeout;
    private final int connectionTimeout;
    private final int maxRedirects;

    public SystemPropertiesConnectionConfig() {
        socketTimeout = Integer.getInteger(HTTP_SOCKET_TIMEOUT_PROPERTY_NAME, DEFAULT_SOCKET_TIMEOUT);
        connectionTimeout = Integer.getInteger(HTTP_CONNECTION_TIMEOUT_PROPERTY_NAME, DEFAULT_CONNECTION_TIMEOUT);
        maxRedirects = Integer.getInteger(HTTP_MAX_REDIRECTS_PROPERTY_NAME, DEFAULT_MAX_REDIRECTS);
    }

    @Override
    public int getSocketTimeout() {
        return socketTimeout;
    }

    @Override
    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    @Override
    public int getMaxRedirects() {
        return maxRedirects;
    }

}
