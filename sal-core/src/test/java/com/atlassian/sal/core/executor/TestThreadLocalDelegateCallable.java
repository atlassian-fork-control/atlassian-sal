package com.atlassian.sal.core.executor;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.google.common.base.Throwables;
import org.junit.Test;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertNotNull;

public class TestThreadLocalDelegateCallable {

    @Test
    public void testRun() throws InterruptedException {
        final ThreadLocalContextManager<Object> manager = new StubThreadLocalContextManager();
        Callable<Void> delegate = new Callable<Void>() {
            public Void call() {
                assertNotNull(manager.getThreadLocalContext());
                return null;
            }
        };

        manager.setThreadLocalContext(new Object());
        final Callable callable = new ThreadLocalDelegateCallable<Object, Void>(manager, delegate);
        Thread t = new Thread(new Runnable() {

            public void run() {
                try {
                    callable.call();
                } catch (Exception e) {
                    throw Throwables.propagate(e);
                }
            }
        });
        t.start();
        t.join(10000);
        assertNotNull(manager.getThreadLocalContext());
    }
}