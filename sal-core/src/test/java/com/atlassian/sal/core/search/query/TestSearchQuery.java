package com.atlassian.sal.core.search.query;

import com.atlassian.sal.api.search.parameter.SearchParameter;
import com.atlassian.sal.api.search.query.SearchQuery;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestSearchQuery {

    @Test
    public void testCreateQuery() {
        SearchQuery query = new DefaultSearchQueryParser().parse("test%20Query");
        query.setParameter(SearchParameter.MAXHITS, "12").
                setParameter("app", "Fisheye");
        assertEquals("test%20Query&maxhits=12&app=Fisheye", query.buildQueryString());
    }

    @Test
    public void testCreateQueryNoParams() {
        SearchQuery query = new DefaultSearchQueryParser().parse("testQuery");
        assertEquals("testQuery", query.buildQueryString());
    }

    @Test
    public void testCreateQuerySpecialChars() {
        SearchQuery query = new DefaultSearchQueryParser().parse("test%25%5E%23Query");
        query.setParameter(SearchParameter.MAXHITS, "12").
                setParameter("ap!@#p", "Fis%^#heye");
        assertEquals("test%25%5E%23Query&maxhits=12&ap%21%40%23p=Fis%25%5E%23heye", query.buildQueryString());
    }

    @Test
    public void testAppendCreateQuerySpecialChars() {
        SearchQuery query = new DefaultSearchQueryParser().parse("test%20Query&maxhits=12&app=Fisheye&a=b");
        query.setParameter(SearchParameter.MAXHITS, "10").
                setParameter("a", "c").append("%20and%20some%20more&app=Crucible");
        assertEquals("test%20Query%20and%20some%20more&maxhits=10&app=Crucible&a=c", query.buildQueryString());
    }

    @Test
    public void testParseComplexQuery() {
        SearchQuery query = new DefaultSearchQueryParser().parse("simpleQuery&maxhits=12&app=fisheye&");
        assertEquals("fisheye", query.getParameter("app"));
        assertNull(query.getParameter("nonexistent"));
    }

    @Test
    public void testParseQuerySpecialCharacters() {
        SearchQuery query = new DefaultSearchQueryParser().parse("test%25%5E%23Query&maxhits=12&ap!%40%23p=Fis%25%5E%23heye");
        assertEquals("test%^#Query", query.getSearchString());
        assertEquals("12", query.getParameter(SearchParameter.MAXHITS));
        assertEquals("Fis%^#heye", query.getParameter("ap!@#p"));
    }

}
