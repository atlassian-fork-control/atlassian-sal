package com.atlassian.sal.api.user;

import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.StringReader;
import java.io.StringWriter;

import static javax.xml.bind.Marshaller.JAXB_FRAGMENT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestUserKeyXmlAdapter {
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    @Before
    public void setUp() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(TestEntity.class);

        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(JAXB_FRAGMENT, true);

        unmarshaller = jaxbContext.createUnmarshaller();
    }

    @Test
    public void xmlAdapterMarshalsAndUnMarshalsCorrectly() throws Exception {
        final TestEntity testEntity = new TestEntity();
        testEntity.userKey = new UserKey("abc");

        final StringWriter buffer = new StringWriter();
        marshaller.marshal(testEntity, buffer);

        assertThat(buffer.toString(), is("<testEntity><userKey>abc</userKey></testEntity>"));

        final TestEntity unmarshalledEntity = (TestEntity) unmarshaller.unmarshal(new StringReader(buffer.toString()));

        assertThat(unmarshalledEntity.userKey, is(new UserKey("abc")));
    }

    @XmlRootElement
    public static class TestEntity {
        @XmlJavaTypeAdapter(UserKeyXmlAdapter.class)
        @XmlElement
        UserKey userKey;
    }
}
