package com.atlassian.sal.api.features;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestValidFeatureKeyPredicate {
    @Test
    public void invalidNullKey() throws Exception {
        assertFalse(ValidFeatureKeyPredicate.isValidFeatureKey(null));
    }

    @Test
    public void invalidEmptyString() throws Exception {
        assertFalse(ValidFeatureKeyPredicate.isValidFeatureKey(""));
    }

    @Test
    public void invalidSpaces() throws Exception {
        assertFalse(ValidFeatureKeyPredicate.isValidFeatureKey(" "));
    }

    @Test
    public void invalidComma() throws Exception {
        assertFalse(ValidFeatureKeyPredicate.isValidFeatureKey(","));
    }

    @Test
    public void validWithDots() throws Exception {
        assertTrue(ValidFeatureKeyPredicate.isValidFeatureKey("com.atlassian.darkfeature.key"));
    }

    @Test
    public void validWithDash() throws Exception {
        assertTrue(ValidFeatureKeyPredicate.isValidFeatureKey("all-the-things"));
    }

    @Test
    public void validWithUnderscore() throws Exception {
        assertTrue(ValidFeatureKeyPredicate.isValidFeatureKey("go_go"));
    }

    @Test
    public void validWithNumbers() throws Exception {
        assertTrue(ValidFeatureKeyPredicate.isValidFeatureKey("v1"));
    }

    @Test
    public void checkValidFeatureKey() {
        final String featureKey = "valid.feature.key";
        final String result = ValidFeatureKeyPredicate.checkFeatureKey(featureKey);
        assertThat(result, is(featureKey));
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void checkInvalidFeatureKey() {
        ValidFeatureKeyPredicate.checkFeatureKey("  invalid   ");
    }
}
